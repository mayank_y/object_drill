module.exports = function pairs(object) {
  let array = [];
  for (let element in object) {
    array.push([element, object[element]]);
  }
  return array;
};

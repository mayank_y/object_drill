function values(object) {
  let values_array = [];
  for (let index in object) {
    values_array.push(object[index]);
  }
  return values_array;
}

module.exports = values;

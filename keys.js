function keys(object) {
  let key_array = [];
  for (let index in object) {
    key_array.push(index);
  }
  return key_array;
}

module.exports = keys;

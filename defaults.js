module.exports = function defaults(object, defaultObject) {
  for (let element in defaultObject) {
    if (!(element in object)) {
      object[element] = defaultObject[element];
    }
  }
  return object;
};

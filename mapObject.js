function mapObject(object, call_back) {
  for (let element in object) {
    let value = object[element];

    call_back(value);
    value = call_back(value);
    object[element] = value;
  }
  return object;
}

module.exports = mapObject;
